package com.hellooop.odj.token.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.hellooop.odj.token.bean.Custom;
import com.hellooop.odj.token.dao.ICustomDao;
import com.hellooop.odj.token.factory.DaoFactory;
import com.hellooop.odj.token.utils.ConstantUtil;
import com.hellooop.odj.token.utils.DaoException;
import com.hellooop.odj.token.utils.DateUtil;
import com.hellooop.odj.token.utils.MD5Utils;
import com.hellooop.odj.token.utils.StringUtil;
import com.hellooop.odj.token.utils.ValidateException;

/**
 * 客户管理类
 *
 * @author lanping
 * @version 1.0
 * @date 2019-9-16 下午7:39:47
 */
public class CustomManager {

	
	/**
     * 获取CustomDao
     * @return
     */
    public ICustomDao getICustomDao(){
        return DaoFactory.getFactory(TokenManager.DEFAULT_DATA_TYPE).getICustomDao();
    }
	
    /**
     * 获取客户
     *
     * @param accessKey
     * @param ipAddress
     * @return
     * @throws DaoException
     */
    public Custom getCustom(String accessKey,String ipAddress) throws DaoException, ValidateException {

        if (StringUtil.isNull(accessKey)) {
            throw new ValidateException("accessKey不能为空");
        }

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("accessKey",accessKey);
        if(StringUtil.isNotNull(ipAddress)){
            map.put("ipAddress",ipAddress);
        }

        ICustomDao customDao = getICustomDao();
        List<Custom> customs = customDao.getCustoms(map);
        if(customs!=null && customs.size()>0){
            return customs.get(0);
        }
        return null;
    }

    /**
     * 获取客户集合
     *
     * @param map
     * @return
     * @throws DaoException
     */
    public List<Custom> getCustoms(Map<String, Object> map) throws DaoException {
        ICustomDao customDao = getICustomDao();
        return customDao.getCustoms(map);
    }

    /**
     * 新增客户
     *
     * @param custom
     * @return
     * @throws DaoException
     */
    public void addCustom(Custom custom) throws DaoException {
        ICustomDao customDao = getICustomDao();
        customDao.addCustom(custom);
    }


    /**
     * 修改客户
     *
     * @param custom
     * @throws DaoException
     */
    public void updateCustom(Custom custom) throws DaoException {
        ICustomDao customDao = getICustomDao();
        customDao.updateCustom(custom);
    }

    /**
     * 校验客户IP地址
     * @param token
     * @param ipAddress
     * @return
     * @throws DaoException
     * @throws ValidateException
     */
    public boolean validCustIPAddress(String token,String ipAddress) throws DaoException, ValidateException {
        boolean flag = false;

        if (StringUtil.isNull(token)) {
            throw new ValidateException("token不能为空");
        }

        ICustomDao customDao = getICustomDao();
        Custom custom = customDao.selectCustomByToken(token);
        if(custom == null){
            throw new ValidateException("该token无效");
        }

        ipAddress = StringUtil.isNull(ipAddress)?"":ipAddress;
        if(ipAddress.equals(custom.getIpAddress())){
            flag = true;
        }

        return flag;
    }
    
    /**
     * 校验accessKey
     * @param custName
     * @param custPass
     * @param ipAddress
     * @return
     * @throws DaoException 
     * @throws ValidateException 
     */
    public boolean validAccessKey(String custName, String custPass, String ipAddress) throws DaoException{
    	boolean flag = true;
    	Map<String, Object> map = new HashMap<String, Object>();
        map.put("custName", custName);
        map.put("custPass", MD5Utils.getMD5(custPass));
        if(StringUtil.isNotNull(ipAddress)){
        	map.put("ipAddress", ipAddress);
        }
        List<Custom> customs = getCustoms(map);
        if (customs != null && customs.size() > 0) {
            Custom custom = customs.get(0);
            if(!custom.valid()){
                flag = false;
            }
        }else{
        	flag = false;
        }
        return flag;
    }


    /**
     * 创建accesskey
     *
     * @param custName  用户名
     * @param custPass  密码
     * @param ipAddress IP地址
     * @param isCreate 是否重新创建
     * @return
     * @throws DaoException 
     */
    public String createAccessKey(String custName, String custPass, String ipAddress,int validYear,
    		boolean isCreate) throws ValidateException, DaoException {
        String accesskey = "";

        //校验是否为空
        if (StringUtil.isNull(custName)) {
            throw new ValidateException("客户名称不能为空");
        }

        if (StringUtil.isNull(custPass)) {
            throw new ValidateException("客户密码不能为空");
        }

        String md5Str = custName + custPass;
        accesskey = MD5Utils.getMD5(md5Str);
        //查询是否存在
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("custName", custName);
        map.put("custPass", MD5Utils.getMD5(custPass));
        List<Custom> customs = getCustoms(map);
        if (customs != null && customs.size() > 0) {
            Custom custom = customs.get(0);
            
            if(!custom.valid()){
            	custom.setIpAddress(ipAddress);
                custom.setUpdateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
                custom.setCustStatus(ConstantUtil.IS_VALID_N);
                updateCustom(custom);
                accesskey = "";
            	throw new ValidateException("系统Token信息已失效，无法创建AccessKey");
            }
        } else {
        	if(!isCreate){
        		throw new ValidateException("系统Token信息无效，无法创建AccessKey");
        	}
        	
        	Custom custom = new Custom();
    		custom.setAccessKey(accesskey);
    		custom.setCustId(UUID.randomUUID().toString());
    		custom.setCustName(custName);
    		custom.setCustPass(MD5Utils.getMD5(custPass));
    		custom.setState(ConstantUtil.IS_VALID_Y);
    		custom.setIpAddress(ipAddress);
    		custom.setCreateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
    		custom.setUpdateTime(DateUtil.getNowDate(DateUtil.DATE_TIME_PATTERN));
    		custom.setCustStatus(ConstantUtil.IS_VALID_Y);
    		custom.setValidYear(validYear);
    		addCustom(custom);
        }
        return accesskey;
    }

}
