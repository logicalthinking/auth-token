package com.hellooop.odj.token.manager;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.token.bean.Custom;
import com.hellooop.odj.token.bean.Token;
import com.hellooop.odj.token.dao.ICustomDao;
import com.hellooop.odj.token.dao.ITokenDao;
import com.hellooop.odj.token.factory.DaoFactory;
import com.hellooop.odj.token.factory.TokenFactory;
import com.hellooop.odj.token.utils.ConstantUtil;
import com.hellooop.odj.token.utils.DaoException;
import com.hellooop.odj.token.utils.DateUtil;
import com.hellooop.odj.token.utils.PropertiesUtil;
import com.hellooop.odj.token.utils.StringUtil;
import com.hellooop.odj.token.utils.ValidateException;

/**
 * tokenManager
 *
 * @author lanping
 * @version 1.0
 * @date 2019-9-16 下午2:29:02
 */
public class TokenManager {

    /**
     * 默认数据库方式
     */
    public final static String DEFAULT_DATA_TYPE = PropertiesUtil.getProperties(ConstantUtil.CONFIG_FILE_PATH)
    		.getProperty(ConstantUtil.DEFAULT_DATA_TYPE);

    /**
     * 获取TokenDao
     * @return
     */
    public ITokenDao getITokenDao(){
        return DaoFactory.getFactory(TokenManager.DEFAULT_DATA_TYPE).getITokenDao();
    }

    /**
     * 获取CustomDao
     * @return
     */
    public ICustomDao getICustomDao(){
        return DaoFactory.getFactory(TokenManager.DEFAULT_DATA_TYPE).getICustomDao();
    }

    /**
     * 获取Token集合
     *
     * @param map
     * @return
     * @throws DaoException
     */
    public List<Token> getTokens(Map<String, Object> map) throws DaoException {
        ITokenDao tokenDao = getITokenDao();
        return tokenDao.getTokens(map);
    }

    /**
     * 获取单个Token对象
     *
     * @param token
     * @return
     * @throws DaoException
     */
    public Token getToken(String token) throws DaoException {
        ITokenDao tokenDao = getITokenDao();
        return tokenDao.getToken(token);
    }

    /**
     * 创建token
     *
     * @param accessKey    访问accessKey
     * @param relativeTime 相对时间 多长时间失效 默认7200秒（两个小时）
     * @param tokenData    json格式数据
     * @param callbackUrl  回调地址
     * @param ipAddress  访问IP地址 可不传
     * @return
     * @throws ValidateException
     * @throws DaoException
     */
    public String createToken(String accessKey,
                              long relativeTime, String tokenData, String callbackUrl,String ipAddress) throws ValidateException,DaoException{
        String token = "";
        //校验是否为空
        if (StringUtil.isNull(accessKey)) {
            throw new ValidateException("accessKey不能为空");
        }

        //通过accessKey 查询校验accessKey是否有效 ，并获取客户信息
        CustomManager customManager = new CustomManager();
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("accessKey", accessKey);
        if(StringUtil.isNotNull(ipAddress)){
            map.put("ipAddress", ipAddress);
        }
        map.put("state", ConstantUtil.IS_VALID_Y);
        List<Custom> customs = customManager.getCustoms(map);
        if (customs == null || customs.size() <= 0
        		|| !customs.get(0).valid()) {
            throw new ValidateException("该accessKey无效！");
        }

        String custId = customs.get(0).getCustId();
        token = UUID.randomUUID().toString();

        //新增token
        Token tokenObj = TokenFactory.getToken(DEFAULT_DATA_TYPE);
        tokenObj.setTokenId(UUID.randomUUID().toString());
        tokenObj.setTokenVal(token);
        tokenObj.setTokenData(tokenData);
        tokenObj.setCustId(custId);
        tokenObj.setState(ConstantUtil.IS_VALID_Y);
        tokenObj.setCreateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
        tokenObj.setUpdateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
        tokenObj.setExpireStartTime(DateUtil.formatDate(DateUtil.getNewDate(new Date(), -1), DateUtil.DATE_TIME_PATTERN));
        tokenObj.setExpireEndTime(DateUtil.formatDate(DateUtil.getNewDate(new Date(), relativeTime), DateUtil.DATE_TIME_PATTERN));
        tokenObj.setRelativeTime(relativeTime);
        tokenObj.setCallbackUrl(callbackUrl);

        ITokenDao tokenDao = getITokenDao();
        tokenDao.addToken(tokenObj);

        return token;
    }

    /**
     * 校验令牌
     * @param token
     * @return
     * @throws DaoException
     * @throws ValidateException
     */
    public boolean validateToken(String token) throws DaoException,ValidateException {
        boolean flag = false;

        //校验是否为空
        if (StringUtil.isNull(token)) {
            throw new ValidateException("token不能为空");
        }

        Token tokenObj = getToken(token);
        if (tokenObj != null) {
            flag = tokenObj.validate();
            if(flag){
            	ICustomDao iCustomDao = new CustomManager().getICustomDao();
            	Custom custom = iCustomDao.getCustom(tokenObj.getCustId());
            	if(custom==null || !custom.valid()){
            		return false;
            	}
            	
                tokenObj.continueLife();
                //修改SQL
                ITokenDao tokenDao = getITokenDao();
                tokenObj.setUpdateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
                tokenDao.updateToken(tokenObj);
            }
        }
        return flag;
    }

    /**
     * 清除令牌
     *
     * @param token
     * @param accessKey 
     * @param ipAddress ip地址 可空
     * @throws DaoException
     * @throws ValidateException
     */
    public void cleanToken(String token,String accessKey,String ipAddress) throws DaoException,ValidateException {

        //校验是否为空
        if (StringUtil.isNull(token)) {
            throw new ValidateException("token不能为空");
        }

        if (StringUtil.isNull(accessKey)) {
            throw new ValidateException("accessKey不能为空");
        }

        Map<String,Object> map = new HashMap<String,Object>();
        map.put("accessKey",accessKey);
        if(StringUtil.isNotNull(ipAddress)){
            map.put("ipAddress",ipAddress);
        }

        ICustomDao customDao = getICustomDao();
        List<Custom> customs = customDao.getCustoms(map);
        if(customs==null || customs.size()<0){
            throw new ValidateException("该accessKey无效");
        }

        Token tokenObj = getToken(token);
        if (tokenObj != null) {
            ITokenDao tokenDao = getITokenDao();
            tokenDao.deleteToken(token);
        }
    }

    /**
     * 定时任务中 获取到过期token对象后清除令牌，需要token与客户ID custId
     * @param token
     * @param custId 客户ID
     * @throws DaoException
     * @throws ValidateException
     */
    public void cleanToken(String token,String custId) throws DaoException,ValidateException {
        //校验是否为空
        if (StringUtil.isNull(token)) {
            throw new ValidateException("token不能为空");
        }

        if (StringUtil.isNull(custId)) {
            throw new ValidateException("custId不能为空");
        }

        ICustomDao customDao = getICustomDao();
        Custom custom = customDao.getCustom(custId);
        if(custom!=null){
            Token tokenObj = getToken(token);
            if (tokenObj != null) {
                ITokenDao tokenDao = getITokenDao();
                tokenDao.deleteToken(token);
            }
        }
    }

    /**
     * 获取token数据
     *
     * @param token
     * @param key
     * @return
     * @throws DaoException 
     * @throws ValidateException
     */
    public String getTokenData(String token, String key) throws DaoException, ValidateException{

        //校验是否为空
        if (StringUtil.isNull(token)) {
            throw new ValidateException("token不能为空");
        }

        if (StringUtil.isNull(key)) {
            throw new ValidateException("key不能为空");
        }

        Token tokenObj = getToken(token);
        if (tokenObj != null) {

            boolean flag = tokenObj.validate();
            if (!flag) {
                throw new ValidateException("该token已失效！");
            }

            return tokenObj.getData(key);
        }
        return null;
    }

    /**
     * 设置token数据
     *
     * @param token
     * @param key
     * @param value
     * @throws DaoException 
     * @throws ValidateException
     */
    public void setTokenData(String token, String key, String value) throws ValidateException, DaoException {

        //校验是否为空
        if (StringUtil.isNull(token)) {
            throw new ValidateException("token不能为空");
        }

        if (StringUtil.isNull(key)) {
            throw new ValidateException("key不能为空");
        }

        Token tokenObj = getToken(token);
        if (tokenObj == null) {
            throw new ValidateException("该token无效！");
        }

        boolean flag = tokenObj.validate();
        if (!flag) {
            throw new ValidateException("该token已失效！");
        }

        tokenObj.setData(key, value);
        //修改SQL
        ITokenDao tokenDao = getITokenDao();
        tokenObj.setUpdateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
        tokenDao.updateToken(tokenObj);
    }

    /**
     * 设置token全部数据
     *
     * @param token
     * @param tokenData json格式字符串
     * @throws DaoException
     * @throws ValidateException
     */
    public void setTokenData(String token, String tokenData) throws ValidateException, DaoException {

        //校验是否为空
        if (StringUtil.isNull(token)) {
            throw new ValidateException("token不能为空");
        }

        if (StringUtil.isNull(tokenData)) {
            throw new ValidateException("tokenData不能为空");
        }

        Token tokenObj = getToken(token);
        if (tokenObj != null) {

            boolean flag = tokenObj.validate();
            if (!flag) {
                throw new ValidateException("该token已失效！");
            }

            tokenObj.setTokenData(tokenData);
            //修改SQL
            ITokenDao tokenDao = getITokenDao();
            tokenObj.setUpdateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
            tokenDao.updateToken(tokenObj);
        }
    }

    /**
     * 获取token全部数据
     *
     * @param token
     * @return
     * @throws DaoException
     * @throws ValidateException
     */
    public JSONObject getTokenData(String token) throws ValidateException, DaoException {
        JSONObject jsonObject = null;
        //校验是否为空
        if (StringUtil.isNull(token)) {
            throw new ValidateException("token不能为空");
        }

        Token tokenObj = getToken(token);
        if (tokenObj != null) {

            boolean flag = tokenObj.validate();
            if (!flag) {
                throw new ValidateException("该token已失效！");
            }

            String tokenData = tokenObj.getTokenData();
            if(StringUtil.isNotNull(tokenData)){
                jsonObject = JSONObject.parseObject(tokenData);
            }
        }
        return jsonObject;
    }

    /**
     * 清除token数据
     *
     * @param token
     * @param key
     * @return
     * @throws DaoException
     * @throws ValidateException
     */
    public void cleanTokenData(String token,String key) throws ValidateException, DaoException {
        //校验是否为空
        if (StringUtil.isNull(token)) {
            throw new ValidateException("token不能为空");
        }

        if (StringUtil.isNull(key)) {
            throw new ValidateException("key不能为空");
        }

        Token tokenObj = getToken(token);
        if (tokenObj != null) {

            boolean flag = tokenObj.validate();
            if (!flag) {
                throw new ValidateException("该token已失效！");
            }

            String tokenData = tokenObj.getTokenData();
            if(StringUtil.isNotNull(tokenData)){
                JSONObject jsonObject = JSONObject.parseObject(tokenData);
                if(jsonObject.containsKey(key)){
                    jsonObject.remove(key);
                    tokenObj.setTokenData(jsonObject.toJSONString());
                    //修改SQL
                    ITokenDao tokenDao = getITokenDao();
                    tokenObj.setUpdateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
                    tokenDao.updateToken(tokenObj);
                }
            }
        }
    }


    /**
     * 清除token全部数据
     *
     * @param token
     * @return
     * @throws DaoException
     * @throws ValidateException
     */
    public void cleanTokenData(String token) throws ValidateException, DaoException {
        //校验是否为空 
        if (StringUtil.isNull(token)) {
            throw new ValidateException("token不能为空");
        }

        Token tokenObj = getToken(token);
        if (tokenObj != null) {

            boolean flag = tokenObj.validate();
            if (!flag) {
                throw new ValidateException("该token已失效！");
            }

            //修改SQL
            ITokenDao tokenDao = getITokenDao();
            tokenObj.setUpdateTime(DateUtil.formatDate(new Date(), DateUtil.DATE_TIME_PATTERN));
            tokenObj.setTokenData("{}");
            tokenDao.updateToken(tokenObj);
        }
    }
}
