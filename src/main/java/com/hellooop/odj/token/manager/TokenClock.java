package com.hellooop.odj.token.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import com.hellooop.odj.token.bean.Token;
import com.hellooop.odj.token.utils.ConstantUtil;
import com.hellooop.odj.token.utils.DaoException;
import com.hellooop.odj.token.utils.PropertiesUtil;
import com.hellooop.odj.token.utils.ValidateException;

/**
 * 校验token定时任务
 * @author lanping
 * @date 2019-9-16 下午1:00:17
 * @version 1.0
 */
public class TokenClock {
	
	private TokenManager tokenManager;
	
	private Loop loop;
	
	public TokenClock(){
		tokenManager = new TokenManager();
	}

	@SuppressWarnings("deprecation")
	public void start() {
		if (loop != null) {
			loop.stopRun();
			loop.stop();
		}
		loop = new Loop(this.tokenManager);
		loop.start();
	}

	@SuppressWarnings("deprecation")
	public void stop() {
		if (loop != null) {
			loop.stopRun();
			loop.stop();
		}
	}
}

class Loop extends Thread {
	
	/**
	 * 线程运行标志
	 */
	private boolean isRun = false;
	
	/**
	 * 睡眠时间 默认 30秒
	 */
	private long RUN_INTERVAL_TIME = 30;
	
	private TokenManager tokenManager;
	
	public void stopRun(){
		isRun = false;
	}
	
	public Loop(TokenManager tokenManager) {
		this.tokenManager = tokenManager;
		Properties properties = PropertiesUtil.getProperties(ConstantUtil.CONFIG_FILE_PATH);
		this.RUN_INTERVAL_TIME = Long.parseLong(properties.getProperty(ConstantUtil.RUN_INTERVAL_TIME));
	}

	public void run() {
		
		while (isRun) {
			try {
				List<Token> list = tokenManager.getTokens(new HashMap<String,Object>());
				System.out.println("启动令牌扫描....");
				System.out.println("find tokens size = "+ (list!=null?list.size():0));
				for (Token token : list) {
					if (!token.validate()) {
						tokenManager.cleanToken(token.getTokenVal(),token.getCustId());
						System.out.println("---------已经过期被清除的令牌-------------");
						System.out.println(token.toString());
						System.out.println("*************************************");
					}
				}
				System.out.println("结束令牌扫描");
				try {
					Thread.sleep(RUN_INTERVAL_TIME*1000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			catch (DaoException e) {
				e.printStackTrace();
			} catch (ValidateException e) {
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public synchronized void start() {
		isRun = true;
		super.start();
	}

}
