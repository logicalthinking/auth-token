package com.hellooop.odj.token.bean;

import java.util.Date;

import com.hellooop.odj.token.utils.ConstantUtil;
import com.hellooop.odj.token.utils.DateUtil;

/**
 * 客户信息
 *
 * @author 兰平
 * @version 1.0
 * @date 2019-09-16
 */
public class Custom {

    private String custId;    //客户ID 主键
    private String custName;    //用户名
    private String custPass;    //密码
    private String accessKey;    //accesskey
    private String ipAddress;    //ip地址
    private String state;    //数据状态 Y 有效 N 无效
    private String custStatus;    //客户状态 Y 有效 N 无效
    private String createTime;    //创建时间
    private String updateTime;	//更新时间
    private Integer validYear;    //有效年限
    
    public String getCustStatus() {
		return custStatus;
	}

	public void setCustStatus(String custStatus) {
		this.custStatus = custStatus;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public Integer getValidYear() {
		return validYear;
	}

	public void setValidYear(Integer validYear) {
		this.validYear = validYear;
	}

	public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getCustPass() {
        return custPass;
    }

    public void setCustPass(String custPass) {
        this.custPass = custPass;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
    
    
    public boolean valid(){
    	boolean result = true;
    	if(ConstantUtil.IS_VALID_N.equals(this.getCustStatus())){
    		return false;
        }
        
        //判断是否有效
        int year = this.getValidYear()==null|| this.getValidYear()==0?1:this.getValidYear();
        int day = DateUtil.getDifferDay(DateUtil.parseDate(this.getCreateTime(), DateUtil.DATE_TIME_PATTERN), new Date());
        if(day > year*ConstantUtil.DAYS_OF_ONE_YAER){
        	result = false;
        }
        return result;
    }

    @Override
    public String toString() {
        return "Custom [custId=" + custId + ",custName=" + custName + ",custPass=" + custPass + ",accessKey=" + accessKey + ",ipAddress=" + ipAddress + ",state=" + state + "]";
    }
}