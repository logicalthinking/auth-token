package com.hellooop.odj.token.bean;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.token.utils.ConstantUtil;
import com.hellooop.odj.token.utils.DateUtil;
import com.hellooop.odj.token.utils.StringUtil;

import java.util.Date;

/**
 * redis token
 *
 * @author lanping
 * @version 1.0
 * @date 2019-9-16 下午12:57:39
 */
public class TokenRedis extends Token {

    /**
     * 回调通知
     */
    @Override
    public void notice() {
        // TODO Auto-generated method stub

    }

    /**
     * 设置data
     * @param key
     * @param value
     */
    @Override
    public void setData(String key, String value) {
        JSONObject jsonObject = null;

        String tokenData = this.getTokenData();
        if (StringUtil.isNotNull(tokenData)) {
            try {
                jsonObject = (JSONObject) JSON.parse(tokenData);
            } catch (Exception e) {
                jsonObject = new JSONObject();
                e.printStackTrace();
            }
        } else {
            jsonObject = new JSONObject();
        }

        jsonObject.put(key, value);
        this.setTokenData(jsonObject.toJSONString());
    }

    /**
     * 获取data
     * @param key
     * @return
     */
    @Override
    public String getData(String key) {
        String tokenData = this.getTokenData();
        if (StringUtil.isNotNull(tokenData)) {
            JSONObject jsonObject = JSON.parseObject(tokenData);
            return jsonObject.getString(key);
        }
        return null;
    }

    /**
     * 延长有效期
     */
    @Override
    public void continueLife() {
        //开始时间改为当前时间减1秒，截止时间改为当前时间加相当过期时间
        this.setExpireStartTime(DateUtil.formatDate(DateUtil.getNewDate(new Date(), -1), DateUtil.DATE_TIME_PATTERN));
        this.setExpireEndTime(DateUtil.formatDate(DateUtil.getNewDate(new Date(), this.getRelativeTime()), DateUtil.DATE_TIME_PATTERN));
    }

    /**
     * 校验token
     * @return
     */
    @Override
    public boolean validate() {
        boolean flag = false;
        if (ConstantUtil.IS_VALID_N.equals(this.getState())) {
            return flag;
        }
        long nowTime = new Date().getTime();
        long startTime = StringUtil.isNull(this.getExpireStartTime()) ? 0L : DateUtil.parseDate(this.getExpireStartTime(), DateUtil.DATE_TIME_PATTERN).getTime();
        long endTime = StringUtil.isNull(this.getExpireEndTime()) ? 0L : DateUtil.parseDate(this.getExpireEndTime(), DateUtil.DATE_TIME_PATTERN).getTime();
        if (startTime <= nowTime && nowTime <= endTime) {
            flag = true;
        }
        return flag;
    }

}