package com.hellooop.odj.token.bean;


/**
 * token信息
 *
 * @author lanping
 * @version 1.0
 * @date 2019-9-16 下午12:57:21
 */
public abstract class Token {

    /**
     * UUID
     */
    private String tokenId;

    /**
     * 客户ID
     */
    private String custId;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 修改时间
     */
    private String updateTime;

    /**
     * token值
     */
    private String tokenVal;

    /**
     * data json格式
     */
    private String tokenData;

    /**
     * 有效期开始时间
     */
    private String expireStartTime;

    /**
     * 有效期截止时间
     */
    private String expireEndTime;

    /**
     * 回调地址
     */
    private String callbackUrl;

    /**
     * 有效时长（秒） 默认7200秒
     */
    private Long relativeTime = 7200L;

    /**
     * 有效状态 Y 有效  N 无效
     */
    private String state = "Y";

    /**
     * 备注
     */
    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getTokenId() {
        return tokenId;
    }

    public void setTokenId(String tokenId) {
        this.tokenId = tokenId;
    }

    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTokenVal() {
        return tokenVal;
    }

    public void setTokenVal(String tokenVal) {
        this.tokenVal = tokenVal;
    }

    public String getTokenData() {
        return tokenData;
    }

    public void setTokenData(String tokenData) {
        this.tokenData = tokenData;
    }

    public String getExpireStartTime() {
        return expireStartTime;
    }

    public void setExpireStartTime(String expireStartTime) {
        this.expireStartTime = expireStartTime;
    }

    public String getExpireEndTime() {
        return expireEndTime;
    }

    public void setExpireEndTime(String expireEndTime) {
        this.expireEndTime = expireEndTime;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }

    public Long getRelativeTime() {
        return relativeTime;
    }

    public void setRelativeTime(Long relativeTime) {
        this.relativeTime = relativeTime;
    }

    public Token() {

    }

    public Token(String tokenVal, String tokenData, String expireStartTime,
                 String expireEndTime, String callbackUrl, Long relativeTime) {
        super();
        this.tokenVal = tokenVal;
        this.tokenData = tokenData;
        this.expireStartTime = expireStartTime;
        this.expireEndTime = expireEndTime;
        this.callbackUrl = callbackUrl;
        this.relativeTime = relativeTime;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((callbackUrl == null) ? 0 : callbackUrl.hashCode());
        result = prime * result
                + ((createTime == null) ? 0 : createTime.hashCode());
        result = prime * result + ((custId == null) ? 0 : custId.hashCode());
        result = prime * result
                + ((expireEndTime == null) ? 0 : expireEndTime.hashCode());
        result = prime * result
                + ((expireStartTime == null) ? 0 : expireStartTime.hashCode());
        result = prime * result
                + ((relativeTime == null) ? 0 : relativeTime.hashCode());
        result = prime * result + ((remark == null) ? 0 : remark.hashCode());
        result = prime * result + ((state == null) ? 0 : state.hashCode());
        result = prime * result
                + ((tokenData == null) ? 0 : tokenData.hashCode());
        result = prime * result + ((tokenId == null) ? 0 : tokenId.hashCode());
        result = prime * result
                + ((tokenVal == null) ? 0 : tokenVal.hashCode());
        result = prime * result
                + ((updateTime == null) ? 0 : updateTime.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Token other = (Token) obj;
        if (callbackUrl == null) {
            if (other.callbackUrl != null)
                return false;
        } else if (!callbackUrl.equals(other.callbackUrl))
            return false;
        if (createTime == null) {
            if (other.createTime != null)
                return false;
        } else if (!createTime.equals(other.createTime))
            return false;
        if (custId == null) {
            if (other.custId != null)
                return false;
        } else if (!custId.equals(other.custId))
            return false;
        if (expireEndTime == null) {
            if (other.expireEndTime != null)
                return false;
        } else if (!expireEndTime.equals(other.expireEndTime))
            return false;
        if (expireStartTime == null) {
            if (other.expireStartTime != null)
                return false;
        } else if (!expireStartTime.equals(other.expireStartTime))
            return false;
        if (relativeTime == null) {
            if (other.relativeTime != null)
                return false;
        } else if (!relativeTime.equals(other.relativeTime))
            return false;
        if (remark == null) {
            if (other.remark != null)
                return false;
        } else if (!remark.equals(other.remark))
            return false;
        if (state == null) {
            if (other.state != null)
                return false;
        } else if (!state.equals(other.state))
            return false;
        if (tokenData == null) {
            if (other.tokenData != null)
                return false;
        } else if (!tokenData.equals(other.tokenData))
            return false;
        if (tokenId == null) {
            if (other.tokenId != null)
                return false;
        } else if (!tokenId.equals(other.tokenId))
            return false;
        if (tokenVal == null) {
            if (other.tokenVal != null)
                return false;
        } else if (!tokenVal.equals(other.tokenVal))
            return false;
        if (updateTime == null) {
            if (other.updateTime != null)
                return false;
        } else if (!updateTime.equals(other.updateTime))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "Token [tokenId=" + tokenId + ", custId=" + custId
                + ", createTime=" + createTime + ", updateTime=" + updateTime
                + ", tokenVal=" + tokenVal + ", tokenData=" + tokenData
                + ", expireStartTime=" + expireStartTime + ", expireEndTime="
                + expireEndTime + ", callbackUrl=" + callbackUrl
                + ", relativeTime=" + relativeTime + ", state=" + state
                + ", remark=" + remark + "]";
    }

    /**
     * 回调通知
     */
    public abstract void notice();

    /**
     * 设置data
     */
    public abstract void setData(String key, String value);

    /**
     * 获取data
     *
     * @param key
     */
    public abstract String getData(String key);

    /**
     * 延长有效期
     */
    public abstract void continueLife();

    /**
     * 校验token
     */
    public abstract boolean validate();

}