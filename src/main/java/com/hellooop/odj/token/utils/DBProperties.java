package com.hellooop.odj.token.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 读取DB配置文件
 *
 * @author 兰平
 * @version 1.0
 * @date 2019-09-16
 */
public class DBProperties extends Properties {

    private static final long serialVersionUID = 1L;
    private static DBProperties instance = null;

    public synchronized static DBProperties getInstance() {
        if (instance != null) {
            return instance;
        } else {
            instance = new DBProperties();
            return instance;
        }
    }

    private DBProperties() {
        InputStream is = DBProperties.class.getResourceAsStream(ConstantUtil.DB_FILE_PATH);
        try {
            this.load(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}