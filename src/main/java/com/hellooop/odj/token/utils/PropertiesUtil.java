package com.hellooop.odj.token.utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * 读取资源配置文件
 */
public class PropertiesUtil {

    public static Properties getProperties(String sourcePath) {
        Properties properties = new Properties();
        try {
            InputStream is = PropertiesUtil.class.getResourceAsStream(sourcePath);
            properties.load(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }

    public static void main(String[] args) {
        Properties properties = PropertiesUtil.getProperties("/redis/redis.properties");
        System.out.println(properties);
        String timeout = properties.getProperty("redis.timeout", "UTF-8");
        System.out.println(timeout);
    }
}
