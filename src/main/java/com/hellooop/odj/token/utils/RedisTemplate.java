package com.hellooop.odj.token.utils;

import java.util.List;
import java.util.Map;

import redis.clients.jedis.Jedis;

/**
 * redis操作类
 *
 * @author lanping
 * @version 1.0
 * @date 2019-9-17 上午10:33:49
 */
public class RedisTemplate {

    /**
     * 字符串set
     *
     * @param key
     * @param value
     */
    public void set(String key, String value) {
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            jedis.set(key, value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
    }

    /**
     * 字符串get
     *
     * @param key
     * @return
     */
    public String get(String key) {
        String value = "";
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            value = jedis.get(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
        return value;
    }

    /**
     * 字符串删除
     *
     * @param key
     */
    public void del(String key) {
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            jedis.del(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
    }

    /**
     * 添加hash类型数据
     *
     * @param key
     * @param map
     */
    public void hmset(String key, Map<String, String> map) {
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            jedis.hmset(key, map);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
    }

    /**
     * 向散列中添加键值
     *
     * @param key
     * @param field
     * @param value
     */
    public void hset(String key, String field, String value) {
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            jedis.hset(key, field, value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
    }

    /**
     * 获取该键包含的所有键值对
     *
     * @param key
     * @return
     */
    public Map<String, String> hget(String key) {
        Map<String, String> map = null;
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            if(jedis.exists(key)){
                map = jedis.hgetAll(key);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
        return map;
    }

    /**
     * 获取散列key中的键field对应值
     *
     * @param key
     * @param field
     * @return
     */
    public String hget(String key, String field) {
        String value = "";
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            value = jedis.hget(key, field);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
        return value;
    }

    /**
     * 删除散列中的键
     *
     * @param key
     * @param field
     */
    public void hdel(String key, String field) {
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            jedis.hdel(key, field);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
    }

    /**
     * 删除散列key中的全部键
     *
     * @param key
     */
    public void hdel(String key) {
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            jedis.hdel(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
    }

    /**
     * 判断散列键是否存在
     *
     * @param key
     * @param field
     * @return
     */
    public boolean hexists(String key, String field) {
        boolean flag = false;
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            flag = jedis.hexists(key, field);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
        return flag;
    }

    /**
     * 获取散列中键的个数
     *
     * @param key
     * @return
     */
    public long hlen(String key) {
        long len = 0L;
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            len = jedis.hlen(key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
        return len;
    }


    /**
     * 添加列表数据
     *
     * @param key
     */
    public void lset(String key, List<String> list) {
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            if (list != null && list.size() > 0) {
                for (String value : list) {
                    jedis.rpush(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
    }

    /**
     * 添加列表数据
     * @param key
     * @param values
     */
    public void lset(String key, String... values) {
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            if (values != null && values.length > 0) {
                for (String value : values) {
                    jedis.rpush(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
    }

    /**
     * 获取列表数据
     *
     * @param key
     * @return
     */
    public List<String> lget(String key) {
        List<String> list = null;
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            list = jedis.lrange(key, 0, -1);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
        return list;
    }

    /**
     * 获取列表索引index数据
     *
     * @param key
     * @return
     */
    public String lget(String key, long index) {
        String value = null;
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            value = jedis.lindex(key, index);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
        return value;
    }

    /**
     * 修改列表指定索引元素，若不存在则报错
     *
     * @param key
     * @param index
     * @param value
     */
    public void lset(String key, long index, String value) {
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            jedis.lset(key, index, value);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
    }

    /**
     * 去除索引范围外的元素
     *
     * @param key
     * @param start
     * @param end
     */
    public void ltrim(String key, long start, long end) {
        Jedis jedis = null;
        try {
            jedis = RedisUtils.getJedis();
            jedis.ltrim(key, start, end);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            RedisUtils.releaseConn(jedis);
        }
    }

    public static void main(String[] args) {
        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.set("userName", "lanping2222");
        String userName = redisTemplate.get("userName");
        System.out.println(userName);
    }
}
