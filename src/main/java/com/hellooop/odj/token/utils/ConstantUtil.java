package com.hellooop.odj.token.utils;

public class ConstantUtil {

	/**
     * config参数默认选择数据存入方式
     */
    public final static String DEFAULT_DATA_TYPE = "DEFAULT_DATA_TYPE";
    
    /**
     * 数据存储类型 sql
     */
    public final static String DATA_TYPE_SQL = "sql";

    /**
     * 数据存储类型 redis
     */
    public final static String DATA_TYPE_REDIS = "redis";
    
    /**
     * 一年默认365天
     */
    public final static int DAYS_OF_ONE_YAER = 365;

    /**
     * 数据有效状态 Y
     */
    public final static String IS_VALID_Y = "Y";

    /**
     * 数据有效状态 N
     */
    public final static String IS_VALID_N = "N";

    /**
     * redis token存入的key
     */
    public final static String REDIS_TOKEN_KEY = "redis.token.key";
    
    /**
     * redis custom存入的key
     */
    public final static String REDIS_CUSTOM_KEY = "redis.custom.key";

    /**
     * 定时任务间隔时间（秒）
     */
    public final static String RUN_INTERVAL_TIME = "RUN_INTERVAL_TIME";
    
    /**
     * config文件路径
     */
    public final static String CONFIG_FILE_PATH = "/config/config.properties";

    /**
     * db文件路径
     */
    public final static String DB_FILE_PATH = "/db/db.properties";
    
    /**
     * redis文件路径
     */
    public final static String REDIS_FILE_PATH = "/redis/redis.properties";
}
