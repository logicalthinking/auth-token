package com.hellooop.odj.token.factory;

import com.hellooop.odj.token.dao.ICustomDao;
import com.hellooop.odj.token.dao.ITokenDao;
import com.hellooop.odj.token.dao.impl.CustomDaoRedis;
import com.hellooop.odj.token.dao.impl.TokenDaoRedis;

/**
 * redisDao工厂
 * @author lanping
 * @version 1.0
 * @date 2019年9月20日
 */
public class RedisDaoFactory extends DaoFactory {

    @Override
    public ITokenDao getITokenDao() {
        return new TokenDaoRedis();
    }

	@Override
	public ICustomDao getICustomDao() {
		return new CustomDaoRedis();
	}

}
