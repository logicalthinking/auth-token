package com.hellooop.odj.token.factory;

import com.hellooop.odj.token.dao.ICustomDao;
import com.hellooop.odj.token.dao.ITokenDao;
import com.hellooop.odj.token.utils.ConstantUtil;

/**
 * Dao 工厂
 * @author lanping
 * @version 1.0
 * @date 2019年9月20日
 */
public abstract class DaoFactory {

    public static DaoFactory getFactory(String dataType) {
        if (ConstantUtil.DATA_TYPE_SQL.equals(dataType)) {
            return new SQLDaoFactory();
        } else if (ConstantUtil.DATA_TYPE_REDIS.equals(dataType)) {
            return new RedisDaoFactory();
        } else {
            return null;
        }
    }

    /**
     * 获取ITokenDao
     * @return
     */
    public abstract ITokenDao getITokenDao();
    
    /**
     * 获取ICustomDao
     * @return
     */
    public abstract ICustomDao getICustomDao();

}
