package com.hellooop.odj.token.factory;

import com.hellooop.odj.token.bean.Token;
import com.hellooop.odj.token.bean.TokenRedis;
import com.hellooop.odj.token.bean.TokenSQL;
import com.hellooop.odj.token.utils.ConstantUtil;

/**
 * Token对象工厂
 *
 * @author lanping
 * @version 1.0
 * @date 2019-9-16 下午1:00:10
 */
public class TokenFactory {

    public static Token getToken(String dataType) {
        if (ConstantUtil.DATA_TYPE_SQL.equals(dataType)) {
            return new TokenSQL();
        } else if (ConstantUtil.DATA_TYPE_REDIS.equals(dataType)) {
            return new TokenRedis();
        } else {
            return null;
        }
    }
}
