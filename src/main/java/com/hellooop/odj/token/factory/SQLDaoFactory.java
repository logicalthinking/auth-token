package com.hellooop.odj.token.factory;

import com.hellooop.odj.token.dao.ICustomDao;
import com.hellooop.odj.token.dao.ITokenDao;
import com.hellooop.odj.token.dao.impl.CustomDaoSQL;
import com.hellooop.odj.token.dao.impl.TokenDaoSQL;

/**
 * sqlDao工厂
 * @author lanping
 * @version 1.0
 * @date 2019年9月20日
 */
public class SQLDaoFactory extends DaoFactory {

    @Override
    public ITokenDao getITokenDao() {
        return new TokenDaoSQL();
    }

	@Override
	public ICustomDao getICustomDao() {
		return new CustomDaoSQL();
	}

}
