package com.hellooop.odj.token.dao.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.token.bean.Token;
import com.hellooop.odj.token.bean.TokenSQL;
import com.hellooop.odj.token.dao.ITokenDao;
import com.hellooop.odj.token.utils.ConstantUtil;
import com.hellooop.odj.token.utils.DaoException;
import com.hellooop.odj.token.utils.RedisTemplate;
import com.hellooop.odj.token.utils.StringUtil;

public class TokenDaoRedis implements ITokenDao {

    @Override
    public void addToken(Token token) throws DaoException {
        JSONObject jsonObject = (JSONObject) JSON.toJSON(token);
        RedisTemplate redisTemplate = new RedisTemplate();
        Map<String,String> map = redisTemplate.hget(ConstantUtil.REDIS_TOKEN_KEY);
        if(map!=null){
            redisTemplate.hset(ConstantUtil.REDIS_TOKEN_KEY,token.getTokenVal(),jsonObject.toJSONString());
        }else{
            map = new HashMap<String,String>();
            map.put(token.getTokenVal(),jsonObject.toJSONString());
            redisTemplate.hmset(ConstantUtil.REDIS_TOKEN_KEY,map);
        }
    }

    @Override
    public void updateToken(Token token) throws DaoException {
        RedisTemplate redisTemplate = new RedisTemplate();
        String tokenjson = redisTemplate.hget(ConstantUtil.REDIS_TOKEN_KEY,token.getTokenVal());
        if(StringUtil.isNotNull(tokenjson)){
            JSONObject jsonObject = JSON.parseObject(tokenjson);
            Field [] fields = token.getClass().getDeclaredFields();
            if(fields==null || fields.length==0){
                fields = token.getClass().getSuperclass().getDeclaredFields();
            }
            for(int i=0;i<fields.length;i++){
                fields[i].setAccessible(true);
                String name = fields[i].getName();
                try {
                    Object value = fields[i].get(token);
                    if(value!=null){
                        jsonObject.put(name,value);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            redisTemplate.hset(ConstantUtil.REDIS_TOKEN_KEY,token.getTokenVal(),jsonObject.toJSONString());
        }
    }

    @Override
    public void deleteToken(String token) throws DaoException {
        RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.hdel(ConstantUtil.REDIS_TOKEN_KEY,token);
    }

    @Override
    public Token getToken(String token) throws DaoException {
        Token tokenObj =null;
        RedisTemplate redisTemplate = new RedisTemplate();
        String tokenjson = redisTemplate.hget(ConstantUtil.REDIS_TOKEN_KEY,token);

        if(StringUtil.isNotNull(tokenjson)) {
            tokenObj = JSON.parseObject(tokenjson, TokenSQL.class);
        }
        return tokenObj;
    }

    /**
     * 查询token集合
     * @param map
     * @return
     * @throws DaoException
     */
    @Override
    public List<Token> getTokens(Map<String, Object> map) throws DaoException {
        String state = (String)map.get("state");
        String token = (String)map.get("token");

        List<Token> tokens = new ArrayList<Token>();
        RedisTemplate redisTemplate = new RedisTemplate();
        Map<String,String> redisMap = redisTemplate.hget(ConstantUtil.REDIS_TOKEN_KEY);
        if(redisMap!=null){
            for(Map.Entry<String,String> entry:redisMap.entrySet()){
                String json = entry.getValue();
                if(StringUtil.isNotNull(json)) {
                    Token tokenObj = JSON.parseObject(json,TokenSQL.class);

                    boolean stateFlag = false;
                    boolean tokenFlag = false;
                    
                    if(StringUtil.isNull(state) || StringUtil.isNotNull(state)
                            && state.equals(tokenObj.getState())){
                    	stateFlag = true;
                        
                    }
                    
                    if(StringUtil.isNull(token) || StringUtil.isNotNull(token)
                            && token.equals(tokenObj.getTokenVal())){
                    	tokenFlag = true;
                    }
                    
                    if(stateFlag && tokenFlag){
                    	tokens.add(tokenObj);
                    }
                }
            }
        }
        return tokens;
    }

}
