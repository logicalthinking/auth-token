package com.hellooop.odj.token.dao;

import java.util.List;
import java.util.Map;

import com.hellooop.odj.token.bean.Token;
import com.hellooop.odj.token.utils.DaoException;

/**
 * @author lanping
 * @version 1.0
 * @date 2019-9-16 下午12:57:11
 */
public interface ITokenDao {

    /**
     * 新增token
     *
     * @param token
     */
    public void addToken(Token token) throws DaoException;

    /**
     * 修改token
     *
     * @param token
     */
    public void updateToken(Token token) throws DaoException;

    /**
     * 删除token
     *
     * @param tokenVal
     */
    public void deleteToken(String tokenVal) throws DaoException;

    /**
     * token值获取Token对象
     *
     * @param tokenVal
     * @return
     */
    public Token getToken(String tokenVal) throws DaoException;


    /**
     * 获取Token集合
     *
     * @param map
     * @return
     */
    public List<Token> getTokens(Map<String, Object> map) throws DaoException;

}
