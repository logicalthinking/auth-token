package com.hellooop.odj.token.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.hellooop.odj.token.bean.Token;
import com.hellooop.odj.token.bean.TokenSQL;
import com.hellooop.odj.token.dao.BaseDao;
import com.hellooop.odj.token.dao.ITokenDao;
import com.hellooop.odj.token.utils.DaoException;
import com.hellooop.odj.token.utils.StringUtil;

public class TokenDaoSQL extends BaseDao implements ITokenDao {

    @Override
    public void addToken(Token token) throws DaoException {
        try {
            StringBuffer sb = new StringBuffer();
            List<Object> params = new ArrayList<Object>();
            sb.append("insert into token_info ( ");
            if (StringUtil.isNotNull(token.getTokenId())) {
                sb.append("token_id , ");
            }
            if (StringUtil.isNotNull(token.getCustId())) {
                sb.append("cust_id , ");
            }
            if (StringUtil.isNotNull(token.getTokenVal())) {
                sb.append("token_val , ");
            }
            if (StringUtil.isNotNull(token.getTokenData())) {
                sb.append("token_data , ");
            }
            if (StringUtil.isNotNull(token.getCreateTime())) {
                sb.append("create_time , ");
            }
            if (StringUtil.isNotNull(token.getUpdateTime())) {
                sb.append("update_time , ");
            }
            if (StringUtil.isNotNull(token.getExpireStartTime())) {
                sb.append("expire_start_time , ");
            }
            if (StringUtil.isNotNull(token.getExpireEndTime())) {
                sb.append("expire_end_time , ");
            }
            if (StringUtil.isNotNull(token.getRelativeTime())) {
                sb.append("relative_time , ");
            }
            if (StringUtil.isNotNull(token.getCallbackUrl())) {
                sb.append("callback_url , ");
            }
            if (StringUtil.isNotNull(token.getRemark())) {
                sb.append("remark , ");
            }
            if (StringUtil.isNotNull(token.getState())) {
                sb.append("state , ");
            }
            if (sb.indexOf(",") != -1) {
                sb.deleteCharAt(sb.lastIndexOf(","));
            }
            sb.append(" ) values ( ");
            if (StringUtil.isNotNull(token.getTokenId())) {
                sb.append(" ? , ");
                params.add(token.getTokenId());
            }
            if (StringUtil.isNotNull(token.getCustId())) {
                sb.append(" ? , ");
                params.add(token.getCustId());
            }
            if (StringUtil.isNotNull(token.getTokenVal())) {
                sb.append(" ? , ");
                params.add(token.getTokenVal());
            }
            if (StringUtil.isNotNull(token.getTokenData())) {
                sb.append(" ? , ");
                params.add(token.getTokenData());
            }
            if (StringUtil.isNotNull(token.getCreateTime())) {
                sb.append(" ? , ");
                params.add(token.getCreateTime());
            }
            if (StringUtil.isNotNull(token.getUpdateTime())) {
                sb.append(" ? , ");
                params.add(token.getUpdateTime());
            }
            if (StringUtil.isNotNull(token.getExpireStartTime())) {
                sb.append(" ? , ");
                params.add(token.getExpireStartTime());
            }
            if (StringUtil.isNotNull(token.getExpireEndTime())) {
                sb.append(" ? , ");
                params.add(token.getExpireEndTime());
            }
            if (StringUtil.isNotNull(token.getRelativeTime())) {
                sb.append(" ? , ");
                params.add(token.getRelativeTime());
            }
            if (StringUtil.isNotNull(token.getCallbackUrl())) {
                sb.append(" ? , ");
                params.add(token.getCallbackUrl());
            }
            if (StringUtil.isNotNull(token.getRemark())) {
                sb.append(" ? , ");
                params.add(token.getRemark());
            }
            if (StringUtil.isNotNull(token.getState())) {
                sb.append(" ? , ");
                params.add(token.getState());
            }
            if (sb.indexOf(",") != -1) {
                sb.deleteCharAt(sb.lastIndexOf(","));
            }
            sb.append(" ) ");
            System.out.println(sb.toString());
            singleUpdate(sb.toString(), params);
        } catch (Exception e) {
            throw new DaoException(e, "新增Token时异常...");
        }
    }

    @Override
    public void updateToken(Token token) throws DaoException {
        try {
            StringBuffer sb = new StringBuffer();
            List<Object> params = new ArrayList<Object>();
            sb.append("update token_info set ");
            if (StringUtil.isNotNull(token.getCustId())) {
                sb.append("cust_id = ? , ");
                params.add(token.getCustId());
            }
            if (StringUtil.isNotNull(token.getTokenVal())) {
                sb.append("token_val = ? , ");
                params.add(token.getTokenVal());
            }
            if (StringUtil.isNotNull(token.getTokenData())) {
                sb.append("token_data = ? , ");
                params.add(token.getTokenData());
            }
            if (StringUtil.isNotNull(token.getCreateTime())) {
                sb.append("create_time = ? , ");
                params.add(token.getCreateTime());
            }
            if (StringUtil.isNotNull(token.getUpdateTime())) {
                sb.append("update_time = ? , ");
                params.add(token.getUpdateTime());
            }
            if (StringUtil.isNotNull(token.getExpireStartTime())) {
                sb.append("expire_start_time = ? , ");
                params.add(token.getExpireStartTime());
            }
            if (StringUtil.isNotNull(token.getExpireEndTime())) {
                sb.append("expire_end_time = ? , ");
                params.add(token.getExpireEndTime());
            }
            if (StringUtil.isNotNull(token.getRelativeTime())) {
                sb.append("relative_time = ? , ");
                params.add(token.getRelativeTime());
            }
            if (StringUtil.isNotNull(token.getCallbackUrl())) {
                sb.append("callback_url = ? , ");
                params.add(token.getCallbackUrl());
            }
            if (StringUtil.isNotNull(token.getRemark())) {
                sb.append("remark = ? , ");
                params.add(token.getRemark());
            }
            if (StringUtil.isNotNull(token.getState())) {
                sb.append("state = ? , ");
                params.add(token.getState());
            }
            if (sb.indexOf(",") != -1) {
                sb.deleteCharAt(sb.lastIndexOf(","));
            }
            sb.append(" where token_id = ? ");
            params.add(token.getTokenId());
            System.out.println(sb.toString());
            singleUpdate(sb.toString(), params);
        } catch (Exception e) {
            throw new DaoException(e, "修改token时异常...");
        }
    }

    @Override
    public void deleteToken(String tokenVal) throws DaoException {
        try {
            String sql = "delete from token_info where token_val = ? ";
            List<Object> params = new ArrayList<Object>();
            params.add(tokenVal);
            System.out.println(sql);
            singleUpdate(sql, params);
        } catch (Exception e) {
            throw new DaoException(e, "删除Token时异常...");
        }
    }

    @Override
    public Token getToken(String tokenVal) throws DaoException {
        try {
            String sql = "select * from token_info where token_val = ? ";
            List<Object> params = new ArrayList<Object>();
            params.add(tokenVal);
            System.out.println(sql);
            List<TokenSQL> list = findSingleTable(sql, params, TokenSQL.class);
            if (list != null && list.size() > 0) {
                return list.get(0);
            }
            return null;
        } catch (Exception e) {
            throw new DaoException(e, "查询单个Token时异常...");
        }
    }

    @Override
    public List<Token> getTokens(Map<String, Object> map) throws DaoException {
        try {
            List<Token> tokens = new ArrayList<Token>();
            List<Object> params = new ArrayList<Object>();
            String sql = "select * from token_info where 1=1 ";
            if (map.get("tokenId") != null) {
                sql += " and token_id =? ";
                params.add(map.get("tokenId"));
            }
            if (map.get("custId") != null) {
                sql += " and cust_id =? ";
                params.add(map.get("custId"));
            }
            if (map.get("tokenVal") != null) {
                sql += " and token_val =? ";
                params.add(map.get("tokenVal"));
            }
            if (map.get("token") != null) {
                sql += " and token_val =? ";
                params.add(map.get("token"));
            }
            if (map.get("tokenData") != null) {
                sql += " and token_data =? ";
                params.add(map.get("tokenData"));
            }
            if (map.get("createTime") != null) {
                sql += " and create_time =? ";
                params.add(map.get("createTime"));
            }
            if (map.get("updateTime") != null) {
                sql += " and update_time =? ";
                params.add(map.get("updateTime"));
            }
            if (map.get("expireStartTime") != null) {
                sql += " and expire_start_time =? ";
                params.add(map.get("expireStartTime"));
            }
            if (map.get("expireEndTime") != null) {
                sql += " and expire_end_time =? ";
                params.add(map.get("expireEndTime"));
            }
            if (map.get("relativeTime") != null) {
                sql += " and relative_time =? ";
                params.add(map.get("relativeTime"));
            }
            if (map.get("callbackUrl") != null) {
                sql += " and callback_url =? ";
                params.add(map.get("callbackUrl"));
            }
            if (map.get("remark") != null) {
                sql += " and remark =? ";
                params.add(map.get("remark"));
            }
            if (map.get("state") != null) {
                sql += " and state =? ";
                params.add(map.get("state"));
            }
            if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
                Integer index = (Integer) map.get("pageIndex");
                Integer size = (Integer) map.get("pageSize");
                Integer count = (index - 1) * size;
                sql += " LIMIT " + count + "," + size;
            }
            System.out.println(sql);
            List<TokenSQL> tokenSqls = findSingleTable(sql, params, TokenSQL.class);
            if (tokenSqls != null && tokenSqls.size() > 0) {
                for (Token token : tokenSqls) {
                    tokens.add(token);
                }
            }
            return tokens;
        } catch (Exception e) {
            throw new DaoException(e, "查询Token时异常...");
        }
    }

}
