package com.hellooop.odj.token.dao;

import java.sql.Connection;

import com.hellooop.odj.token.utils.DaoException;

/**
 * 事务处理
 *
 * @author 兰平
 * @version 1.0
 * @date 2019-09-16
 */
public interface ITransaction {

    /**
     * 提交
     */
    public void commit() throws DaoException;

    /**
     * 回滚
     */
    public void rollback() throws DaoException;

    /**
     * 获取连接
     */
    public Connection getConnection() throws DaoException;

    /**
     * 关闭连接
     */
    public void close() throws DaoException;

}