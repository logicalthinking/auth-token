package com.hellooop.odj.token.dao;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.hellooop.odj.token.utils.DBProperties;
import com.hellooop.odj.token.utils.DaoException;
import com.hellooop.odj.token.utils.StringUtil;

/**
 * 数据库操作
 *
 * @author 兰平
 * @version 1.0
 * @date 2019-09-16
 */
public class BaseDao {

    //静态快，用来加载驱动
    static {
        try {
            Class.forName(DBProperties.getInstance().getProperty("SQL_DRIVER"));
        } catch (ClassNotFoundException e) {
            try {
                throw new DaoException(e, "加载SQL驱动时异常...");
            } catch (DaoException e1) {
                e1.printStackTrace();
            }
        }
    }

    /**
     * 获取连接
     */
    public Connection getConnection() throws DaoException {
        Connection con = null;
        try {
            con = DriverManager.getConnection(DBProperties.getInstance()
                    .getProperty("SQL_URL"), DBProperties.getInstance()
                    .getProperty("SQL_USERNAME"), DBProperties.getInstance()
                    .getProperty("SQL_PASSWORD"));
        } catch (SQLException e) {
            throw new DaoException(e, "获取SQL连接时异常...");
        }
        return con;
    }

    /**
     * 关闭的方法
     */
    public void closeAll(Connection con, PreparedStatement pstmt, ResultSet rs) {
        if (rs != null) {
            try {
                rs.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (pstmt != null) {
            try {
                pstmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 设置PreparedStatement对象的sql语句中的参数？
     */
    public void setValues(PreparedStatement pstmt, List<Object> params) throws DaoException {
        if (pstmt != null && params != null && params.size() > 0) {
            for (int i = 0; i < params.size(); i++) {
                try {
                    if (params.get(i) != null) {
                        String paramType = params.get(i).getClass().getName();
                        if ("java.lang.Integer".equals(paramType)) {
                            pstmt.setInt(i + 1, (Integer) params.get(i));
                        } else if ("java.lang.String".equals(paramType)) {
                            pstmt.setString(i + 1, (String) params.get(i));
                        } else if ("java.math.BigDecimal".equals(paramType)) {
                            pstmt.setBigDecimal(i + 1, (BigDecimal) params.get(i));
                        } else if ("java.sql.Timestamp".equals(paramType)) {
                            pstmt.setTimestamp(i + 1, (Timestamp) params.get(i));
                        } else if ("java.lang.Double".equals(paramType)) {
                            pstmt.setDouble(i + 1, (Double) params.get(i));
                        } else if ("java.lang.Float".equals(paramType)) {
                            pstmt.setFloat(i + 1, (Float) params.get(i));
                        } else if ("java.lang.Date".equals(paramType)) {
                            pstmt.setString(i + 1, (String) params.get(i));
                        } else {
                            pstmt.setObject(i + 1, params.get(i));
                        }
                    } else {
                        pstmt.setObject(i + 1, null);
                    }
                } catch (SQLException e) {
                    throw new DaoException(e, "设置参数时异常...");
                }
            }
        }
    }

    /**
     * 多表增删改
     *
     * @param sql    sql语句集合，里面可以加？
     * @param params 参数列表，用来替换sql中的?（占位符）
     * @return int 返回的值。成功>0，失败<=0
     */
    public synchronized int update(List<String> sql, List<List<Object>> params) throws DaoException {
        int result = 0;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection();
            con.setAutoCommit(false); // 事务处理
            for (int i = 0; i < sql.size(); i++) {
                List<Object> param = params.get(i);
                pstmt = con.prepareStatement(sql.get(i)); // 预编译对象
                setValues(pstmt, param); // 设置参数
                pstmt.executeUpdate();
            }
            result = 1;
            con.commit(); // 没有错误执行
        } catch (SQLException e) {
            try {
                result = 0;
                e.printStackTrace();
                con.rollback(); // 出错回滚
            } catch (SQLException e1) {
                e1.printStackTrace();
            }
            throw new DaoException(e, "多表增删改执行SQL语句时异常...");
        } finally {
            try {
                con.setAutoCommit(true);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            closeAll(con, pstmt, rs);
        }
        return result;
    }

    /**
     * 单表增删改
     *
     * @param sql    sql语句，里面可以加？
     * @param params 参数列表，用来替换sql中的?（占位符）
     * @return int 返回的值。成功>0，失败<=0
     */
    public synchronized int singleUpdate(String sql, List<Object> params) throws DaoException {
        int result = 0;
        Connection con = null;
        PreparedStatement pstmt = null;
        try {
            con = getConnection();
            pstmt = con.prepareStatement(sql); // 预编译对象
            setValues(pstmt, params); // 设置参数
            result = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e, "单表增删改执行SQL语句时异常...");
        } finally {
            closeAll(con, pstmt, null);
        }
        return result;
    }

    /**
     * 单表增删改
     *
     * @param sql    sql语句，里面可以加？
     * @param params 参数列表，用来替换sql中的?（占位符）
     * @param conn   数据库连接对象
     * @return int 返回的值。成功>0，失败<=0
     */
    public synchronized int singleUpdate(String sql, List<Object> params,
                                         Connection conn) throws DaoException {
        int result = 0;
        PreparedStatement pstmt = null;
        try {
            pstmt = conn.prepareStatement(sql); // 预编译对象
            setValues(pstmt, params); // 设置参数
            result = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e, "单表增删改执行SQL语句异常...");
        }
        return result;
    }

    /**
     * 单表增删改
     *
     * @param sql    sql语句，里面可以加？
     * @param params 参数列表，用来替换sql中的?（占位符）
     * @param conn   数据库连接对象
     * @return int 返回的值。成功>0，失败<=0
     */
    public synchronized int singleUpdate(String sql, List<Object> params,
                                         ITransaction tran) throws DaoException {
        int result = 0;
        PreparedStatement pstmt = null;
        try {
            Connection conn = tran.getConnection();
            pstmt = conn.prepareStatement(sql); // 预编译对象
            setValues(pstmt, params); // 设置参数
            result = pstmt.executeUpdate();
        } catch (SQLException e) {
            throw new DaoException(e, "单表增删改执行SQL语句异常...");
        }
        return result;
    }

    /**
     * 聚合查询
     *
     * @param sql    sql语句，里面可以加？
     * @param params 参数列表，用来替换sql中的?（占位符）
     * @return list 返回的结果集
     */
    public List<String> uniqueResult(String sql, List<Object> params) throws DaoException {
        List<String> list = new ArrayList<String>();
        Connection con = this.getConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            pstmt = con.prepareStatement(sql); // 预编译对象
            setValues(pstmt, params); // 设置参数
            rs = pstmt.executeQuery();
            ResultSetMetaData md = rs.getMetaData(); // 数据库元数据
            int count = md.getColumnCount(); // 取出结果集中列的数量
            if (rs != null && rs.next()) {
                for (int i = 1; i <= count; i++) {
                    list.add(rs.getString(i));
                }
            }
        } catch (SQLException e) {
            throw new DaoException(e, "聚合查询时异常...");
        } finally {
            closeAll(con, pstmt, rs);
        }
        return list;
    }

    /**
     * 查询单个表
     *
     * @param sql    sql语句，里面可以加？
     * @param params 参数列表，用来替换sql中的?（占位符）
     * @param c      泛型类型所对应的反射对象
     * @return 存储了对象的集合
     */
    public <T> List<T> findSingleTable(String sql, List<Object> params,
                                       Class<T> c) throws DaoException {
        List<T> list = new ArrayList<T>(); // 要返回的结果集
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection(); // 获取连接
            pstmt = con.prepareStatement(sql); // 预编译对象
            setValues(pstmt, params); // 设置参数
            rs = pstmt.executeQuery(); // 执行查询语句，得到结果集

            Method[] ms = c.getMethods(); // 取出这个反射实例的所有方法

            ResultSetMetaData md = rs.getMetaData(); // 获取结果集的元数据
            String[] tableColnames = new String[md.getColumnCount()];
            String[] colnames = new String[md.getColumnCount()];
            for (int i = 0; i < colnames.length; i++) { // 将列名保存到colnames数组中
                tableColnames[i] = md.getColumnLabel(i + 1);
                colnames[i] = StringUtil.toUnderscoreToCamelCase(md.getColumnLabel(i + 1));
            }

            T t;
            String mname = null;
            String cname = null;
            String ctypename = null;
            while (rs.next()) {
                t = (T) c.newInstance(); // 通过反射实例来获取一个对象 然后将数据库中的对象的值 注入到这个对象中
                for (int i = 0; i < colnames.length; i++) {// 循环方法名
                    cname = "set" + colnames[i]; // 取出列名并在前面加上set
                    for (Method m : ms) {// 循环列名
                        mname = m.getName(); // 取出方法名
                        if (cname.equalsIgnoreCase(mname) && rs.getObject(tableColnames[i]) != null) {// 判断方法名和列名是否一样，相同则激活方法，注入数据
                            ctypename = rs.getObject(tableColnames[i]).getClass().getName();// 获取当前列的类型名
                            if ("java.lang.Integer".equals(ctypename)) {
                                m.invoke(t, rs.getInt(tableColnames[i]));// invoke激活
                            } else if ("java.lang.String".equals(ctypename)) {
                                m.invoke(t, rs.getString(tableColnames[i]));
                            } else if ("java.math.BigDecimal".equals(ctypename)) {
                                try {
                                    m.invoke(t, rs.getBigDecimal(tableColnames[i]));
                                } catch (Exception e1) {
                                    m.invoke(t, rs.getDouble(tableColnames[i]));
                                }
                            } else if ("java.sql.Timestamp".equals(ctypename)) {
                                m.invoke(t, rs.getString(tableColnames[i]));
                            } else if ("java.sql.Date".equals(ctypename)) {
                                m.invoke(t, rs.getString(tableColnames[i]));
                            } else if ("java.sql.Time".equals(ctypename)) {
                                m.invoke(t, rs.getString(tableColnames[i]));
                            } else if ("java.lang.Float".equals(ctypename)) {
                                m.invoke(t, rs.getFloat(tableColnames[i]));
                            } else if ("java.lang.Double".equals(ctypename)) {
                                m.invoke(t, rs.getDouble(tableColnames[i]));
                            } else if ("java.lang.Long".equals(ctypename)) {
                                m.invoke(t, rs.getLong(tableColnames[i]));
                            } else {
                                m.invoke(t, rs.getString(tableColnames[i]));
                            }
                            break;
                        }
                    }
                }
                list.add(t);
            }
        } catch (Exception e) {
            throw new DaoException(e, "单表查询时异常...");
        } finally {
            closeAll(con, pstmt, rs);
        }
        return list;
    }

    /**
     * 多表查询
     *
     * @param sql    sql语句，里面可以加？
     * @param params 参数列表，用来替换sql中的?（占位符）
     * @return 结果集，存在一个List表中，用Map一对一的存放
     */
    public List<Map<String, Object>> findResult(String sql, List<Object> params) throws DaoException {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();// 查询的结果集
        Map<String, Object> map;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            con = getConnection(); // 获取连接
            pstmt = con.prepareStatement(sql); // 预编译对象
            setValues(pstmt, params); // 设置参数
            rs = pstmt.executeQuery(); // 执行查询语句，得到结果集

            ResultSetMetaData md = rs.getMetaData(); // 获取结果集的元数据
            String[] colnames = new String[md.getColumnCount()]; // 获取结果集中的列名
            for (int i = 0; i < colnames.length; i++) { // 将列名保存到colnames数组中
                colnames[i] = md.getColumnLabel(i + 1);
            }

            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap<String, Object>();
                    for (int i = 0; i < colnames.length; i++) {// 循环列名
                        if (rs.getObject(colnames[i]) != null) {
                            String ctypename = rs.getObject(colnames[i]).getClass().getName();// 获取当前列的类型名
                            if ("java.lang.Integer".equals(ctypename)) {
                                map.put(colnames[i], rs.getInt(colnames[i]));
                            } else if ("java.lang.String".equals(ctypename)) {
                                map.put(colnames[i], rs.getString(colnames[i]));
                            } else if ("java.math.BigDecimal".equals(ctypename)) {
                                try {
                                    map.put(colnames[i], rs.getBigDecimal(colnames[i]));
                                } catch (Exception e1) {
                                    map.put(colnames[i], rs.getDouble(colnames[i]));
                                }
                            } else if ("java.sql.Timestamp".equals(ctypename)) {
                                map.put(colnames[i], rs.getString(colnames[i]));
                            } else if ("java.sql.Date".equals(ctypename)) {
                                map.put(colnames[i], rs.getString(colnames[i]));
                            } else if ("java.sql.Time".equals(ctypename)) {
                                map.put(colnames[i], rs.getString(colnames[i]));
                            } else if ("java.lang.Float".equals(ctypename)) {
                                map.put(colnames[i], rs.getFloat(colnames[i]));
                            } else if ("java.lang.Double".equals(ctypename)) {
                                map.put(colnames[i], rs.getDouble(colnames[i]));
                            } else {
                                map.put(colnames[i], rs.getString(colnames[i]));
                            }
                        } else {
                            map.put(colnames[i], null);
                        }
                    }
                    list.add(map);
                }
            }
        } catch (SQLException e) {
            throw new DaoException(e, "多表查询时异常...");
        } finally {
            closeAll(con, pstmt, rs);
        }
        return list;
    }

}