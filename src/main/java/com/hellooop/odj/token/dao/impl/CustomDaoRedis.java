package com.hellooop.odj.token.dao.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.token.bean.Custom;
import com.hellooop.odj.token.bean.Token;
import com.hellooop.odj.token.bean.TokenSQL;
import com.hellooop.odj.token.dao.BaseDao;
import com.hellooop.odj.token.dao.ICustomDao;
import com.hellooop.odj.token.utils.ConstantUtil;
import com.hellooop.odj.token.utils.DaoException;
import com.hellooop.odj.token.utils.RedisTemplate;
import com.hellooop.odj.token.utils.StringUtil;

public class CustomDaoRedis extends BaseDao implements ICustomDao {

    @Override
    public void addCustom(Custom custom) throws DaoException {
    	JSONObject jsonObject = (JSONObject) JSON.toJSON(custom);
        RedisTemplate redisTemplate = new RedisTemplate();
        Map<String,String> map = redisTemplate.hget(ConstantUtil.REDIS_CUSTOM_KEY);
        if(map!=null){
            redisTemplate.hset(ConstantUtil.REDIS_CUSTOM_KEY,custom.getCustId(),jsonObject.toJSONString());
        }else{
            map = new HashMap<String,String>();
            map.put(custom.getCustId(),jsonObject.toJSONString());
            redisTemplate.hmset(ConstantUtil.REDIS_CUSTOM_KEY,map);
        }
    }

    @Override
    public void updateCustom(Custom custom) throws DaoException {
    	RedisTemplate redisTemplate = new RedisTemplate();
        String tokenjson = redisTemplate.hget(ConstantUtil.REDIS_CUSTOM_KEY,custom.getCustId());
        if(StringUtil.isNotNull(tokenjson)){
            JSONObject jsonObject = JSON.parseObject(tokenjson);
            Field [] fields = custom.getClass().getDeclaredFields();
            if(fields==null || fields.length==0){
                fields = custom.getClass().getSuperclass().getDeclaredFields();
            }
            for(int i=0;i<fields.length;i++){
                fields[i].setAccessible(true);
                String name = fields[i].getName();
                try {
                    Object value = fields[i].get(custom);
                    if(value!=null){
                        jsonObject.put(name,value);
                    }
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            }
            redisTemplate.hset(ConstantUtil.REDIS_CUSTOM_KEY,custom.getCustId(),jsonObject.toJSONString());
        }
    }

    @Override
    public void deleteCustom(String custId) throws DaoException {
    	RedisTemplate redisTemplate = new RedisTemplate();
        redisTemplate.hdel(ConstantUtil.REDIS_CUSTOM_KEY,custId);
    }

    @Override
    public Custom getCustom(String custId) throws DaoException {
    	Custom custom =null;
         RedisTemplate redisTemplate = new RedisTemplate();
         String custjson = redisTemplate.hget(ConstantUtil.REDIS_CUSTOM_KEY,custId);

         if(StringUtil.isNotNull(custjson)) {
             System.out.println(custjson);
             custom = JSON.parseObject(custjson, Custom.class);
         }
         return custom;
    }

    @Override
    public Custom selectCustomByToken(String token) throws DaoException {
    	Custom custom = null;
    	Token tokenObj =null;
    	
        RedisTemplate redisTemplate = new RedisTemplate();
        //获取token
        String tokenjson = redisTemplate.hget(ConstantUtil.REDIS_TOKEN_KEY,token);

        if(StringUtil.isNotNull(tokenjson)) {
            tokenObj = JSON.parseObject(tokenjson, TokenSQL.class);
        }
        if(tokenObj!=null){
        	String custId = tokenObj.getCustId();
        	custom = getCustom(custId);
        }
    	
    	return custom;
    }

    @Override
    public List<Custom> getCustoms(Map<String, Object> map) throws DaoException {
    	String state = (String)map.get("state");
        String accessKey = (String)map.get("accessKey");
        String custName = (String)map.get("custName");
        String custPass = (String)map.get("custPass");
        String ipAddress = (String)map.get("ipAddress");
        String custStatus = (String)map.get("custStatus");
        String validYear = (String)map.get("validYear");

        List<Custom> customs = new ArrayList<Custom>();
        RedisTemplate redisTemplate = new RedisTemplate();
        Map<String,String> redisMap = redisTemplate.hget(ConstantUtil.REDIS_CUSTOM_KEY);
        if(redisMap!=null){
            for(Map.Entry<String,String> entry:redisMap.entrySet()){
                String json = entry.getValue();
                if(StringUtil.isNotNull(json)) {
                	Custom custom = JSON.parseObject(json,Custom.class);

                	boolean stateFlag = false;
                	boolean accessFlag = false;
                	boolean custNameFlag = false;
                	boolean custPassFlag = false;
                	boolean ipFlag = false;
                	boolean custStatusFlag = false;
                	boolean validYearFlag = false;
                	
                	if(StringUtil.isNull(state) || StringUtil.isNotNull(state)
                            && state.equals(custom.getState()) ){
                		stateFlag = true;
                	}
                	if(StringUtil.isNull(accessKey) || StringUtil.isNotNull(accessKey)
                			&& accessKey.equals(custom.getAccessKey()) ){
                		accessFlag = true;
                	}
                	if(StringUtil.isNull(custName) || StringUtil.isNotNull(custName)
                			&& custName.equals(custom.getCustName()) ){
                		custNameFlag = true;
                	}
                	if(StringUtil.isNull(custPass) || StringUtil.isNotNull(custPass)
                			&& custPass.equals(custom.getCustPass()) ){
                		custPassFlag = true;
                	}
                	if(StringUtil.isNull(ipAddress) || StringUtil.isNotNull(ipAddress)
                			&& ipAddress.equals(custom.getIpAddress()) ){
                		ipFlag = true;
                	}
                	if(StringUtil.isNull(custStatus) || StringUtil.isNotNull(custStatus)
                			&& custStatus.equals(custom.getCustStatus()) ){
                		custStatusFlag = true;
                	}
                	if(StringUtil.isNull(validYear) || StringUtil.isNotNull(validYear)
                			&& validYear.equals(custom.getValidYear()) ){
                		validYearFlag = true;
                	}
                	
                	if(stateFlag && accessFlag && custNameFlag && custPassFlag && ipFlag 
                			&& custStatusFlag && validYearFlag){
                		customs.add(custom);
                	}
                }
            }
        }
        return customs;
    }

}
