package com.hellooop.odj.token.dao;

import java.util.List;
import java.util.Map;

import com.hellooop.odj.token.bean.Custom;
import com.hellooop.odj.token.utils.DaoException;

/**
 * 客户Dao
 *
 * @author lanping
 * @version 1.0
 * @date 2019-9-16 下午7:25:42
 */
public interface ICustomDao {

    /**
     * 新增客户
     *
     * @param custom
     */
    public void addCustom(Custom custom) throws DaoException;

    /**
     * 修改客户
     *
     * @param custom
     */
    public void updateCustom(Custom custom) throws DaoException;

    /**
     * 删除客户
     *
     * @param custId
     */
    public void deleteCustom(String custId) throws DaoException;

    /**
     * 查询单个客户
     *
     * @param custId
     * @return
     */
    public Custom getCustom(String custId) throws DaoException;

    /**
     * 根据token查询客户
     *
     * @param tokenVal
     * @return
     */
    public Custom selectCustomByToken(String token) throws DaoException;

    /**
     * 查询客户集合
     *
     * @param map
     * @return
     */
    public List<Custom> getCustoms(Map<String, Object> map) throws DaoException;
}
