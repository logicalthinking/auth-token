package com.hellooop.odj.token.dao;

import java.sql.Connection;
import java.sql.SQLException;

import com.hellooop.odj.token.utils.DaoException;

/**
 * 事务处理实现类
 *
 * @author 兰平
 * @version 1.0
 * @date 2019-09-16
 */
public class Transaction implements ITransaction {

    private Connection conn;

    public Transaction() throws DaoException {
        this.conn = new BaseDao().getConnection();
        try {
            this.conn.setAutoCommit(false);
        } catch (SQLException e) {
            throw new DaoException(e, "Transaction设置连接AutoCommit时失败...");
        }
    }

    public Transaction(Connection conn) throws DaoException {
        this.conn = conn;
        try {
            this.conn.setAutoCommit(false);
        } catch (SQLException e) {
            throw new DaoException(e, "Transaction设置连接AutoCommit时失败...");
        }
    }

    public void commit() throws DaoException {
        try {
            conn.commit();
            conn.setAutoCommit(true);
            conn.close();
        } catch (SQLException e) {
            throw new DaoException(e, "事务提交时失败...");
        }
    }

    public void rollback() throws DaoException {
        try {
            conn.rollback();
            conn.setAutoCommit(true);
            conn.close();
        } catch (SQLException e) {
            throw new DaoException(e, "事务回滚时失败...");
        }
    }

    public Connection getConnection() throws DaoException {
        return conn;
    }

    public void close() throws DaoException {
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                throw new DaoException(e, "连接关闭时失败...");
            }
        }
    }

}