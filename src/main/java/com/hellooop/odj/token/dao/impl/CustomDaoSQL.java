package com.hellooop.odj.token.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.hellooop.odj.token.bean.Custom;
import com.hellooop.odj.token.dao.BaseDao;
import com.hellooop.odj.token.dao.ICustomDao;
import com.hellooop.odj.token.utils.DaoException;
import com.hellooop.odj.token.utils.StringUtil;

public class CustomDaoSQL extends BaseDao implements ICustomDao {

    @Override
    public void addCustom(Custom custom) throws DaoException {
        try {
            StringBuffer sb = new StringBuffer();
            List<Object> params = new ArrayList<Object>();
            sb.append("insert into token_cust_info ( ");
            if (StringUtil.isNotNull(custom.getCustId())) {
                sb.append("cust_id , ");
            }
            if (StringUtil.isNotNull(custom.getCustName())) {
                sb.append("cust_name , ");
            }
            if (StringUtil.isNotNull(custom.getCustPass())) {
                sb.append("cust_pass , ");
            }
            if (StringUtil.isNotNull(custom.getAccessKey())) {
                sb.append("access_key , ");
            }
            if (StringUtil.isNotNull(custom.getIpAddress())) {
                sb.append("ip_address , ");
            }
            if (StringUtil.isNotNull(custom.getCustStatus())) {
            	sb.append("cust_status , ");
            }
            if (StringUtil.isNotNull(custom.getCreateTime())) {
            	sb.append("create_time , ");
            }
            if (StringUtil.isNotNull(custom.getUpdateTime())) {
            	sb.append("update_time , ");
            }
            if (StringUtil.isNotNull(custom.getValidYear())) {
            	sb.append("valid_year , ");
            }
            if (StringUtil.isNotNull(custom.getState())) {
                sb.append("state , ");
            }
            if (sb.indexOf(",") != -1) {
                sb.deleteCharAt(sb.lastIndexOf(","));
            }
            sb.append(" ) values ( ");
            if (StringUtil.isNotNull(custom.getCustId())) {
                sb.append(" ? , ");
                params.add(custom.getCustId());
            }
            if (StringUtil.isNotNull(custom.getCustName())) {
                sb.append(" ? , ");
                params.add(custom.getCustName());
            }
            if (StringUtil.isNotNull(custom.getCustPass())) {
                sb.append(" ? , ");
                params.add(custom.getCustPass());
            }
            if (StringUtil.isNotNull(custom.getAccessKey())) {
                sb.append(" ? , ");
                params.add(custom.getAccessKey());
            }
            if (StringUtil.isNotNull(custom.getIpAddress())) {
                sb.append(" ? , ");
                params.add(custom.getIpAddress());
            }
            if (StringUtil.isNotNull(custom.getCustStatus())) {
            	sb.append(" ? , ");
            	params.add(custom.getCustStatus());
            }
            if (StringUtil.isNotNull(custom.getCreateTime())) {
            	sb.append(" ? , ");
            	params.add(custom.getCreateTime());
            }
            if (StringUtil.isNotNull(custom.getUpdateTime())) {
            	sb.append(" ? , ");
            	params.add(custom.getUpdateTime());
            }
            if (StringUtil.isNotNull(custom.getValidYear())) {
            	sb.append(" ? , ");
            	params.add(custom.getValidYear());
            }
            if (StringUtil.isNotNull(custom.getState())) {
                sb.append(" ? , ");
                params.add(custom.getState());
            }
            if (sb.indexOf(",") != -1) {
                sb.deleteCharAt(sb.lastIndexOf(","));
            }
            sb.append(" ) ");
            System.out.println(sb.toString());
            singleUpdate(sb.toString(), params);
        } catch (Exception e) {
            throw new DaoException(e, "新增Custom时异常...");
        }
    }

    @Override
    public void updateCustom(Custom custom) throws DaoException {
        try {
            StringBuffer sb = new StringBuffer();
            List<Object> params = new ArrayList<Object>();
            sb.append("update token_cust_info set ");
            if (StringUtil.isNotNull(custom.getCustName())) {
                sb.append("cust_name = ? , ");
                params.add(custom.getCustName());
            }
            if (StringUtil.isNotNull(custom.getCustPass())) {
                sb.append("cust_pass = ? , ");
                params.add(custom.getCustPass());
            }
            if (StringUtil.isNotNull(custom.getAccessKey())) {
                sb.append("access_key = ? , ");
                params.add(custom.getAccessKey());
            }
            if (StringUtil.isNotNull(custom.getIpAddress())) {
                sb.append("ip_address = ? , ");
                params.add(custom.getIpAddress());
            }
            if (StringUtil.isNotNull(custom.getCustStatus())) {
            	sb.append("cust_status=? , ");
            	params.add(custom.getCustStatus());
            }
            if (StringUtil.isNotNull(custom.getCreateTime())) {
            	sb.append("create_time=? , ");
            	params.add(custom.getCreateTime());
            }
            if (StringUtil.isNotNull(custom.getUpdateTime())) {
            	sb.append("update_time=? , ");
            	params.add(custom.getUpdateTime());
            }
            if (StringUtil.isNotNull(custom.getValidYear())) {
            	sb.append("valid_year=? , ");
            	params.add(custom.getValidYear());
            }
            if (StringUtil.isNotNull(custom.getState())) {
                sb.append("state = ? , ");
                params.add(custom.getState());
            }
            if (sb.indexOf(",") != -1) {
                sb.deleteCharAt(sb.lastIndexOf(","));
            }
            sb.append(" where cust_id = ? ");
            params.add(custom.getCustId());
            System.out.println(sb.toString());
            singleUpdate(sb.toString(), params);
        } catch (Exception e) {
            throw new DaoException(e, "修改Custom时异常...");
        }
    }

    @Override
    public void deleteCustom(String custId) throws DaoException {
        try {
            String sql = "delete from token_cust_info where cust_id in ( " + custId + " ) ";
            System.out.println(sql);
            singleUpdate(sql, null);
        } catch (Exception e) {
            throw new DaoException(e, "删除Custom时异常...");
        }

    }

    @Override
    public Custom getCustom(String custId) throws DaoException {
        try {
            String sql = "select * from token_cust_info where cust_id = ? ";
            List<Object> params = new ArrayList<Object>();
            params.add(custId);
            System.out.println(sql);
            List<Custom> list = findSingleTable(sql, params, Custom.class);
            if (list != null && list.size() > 0) {
                return list.get(0);
            }
            return null;
        } catch (Exception e) {
            throw new DaoException(e, "查询单个custom时异常...");
        }
    }

    @Override
    public Custom selectCustomByToken(String token) throws DaoException {
        try {
            String sql = "select cust.* from token_cust_info cust " +
                    "inner join token_info token on cust.cust_id = token.cust_id and token.state='Y' "+
                    " where token.token_val = ? ";
            List<Object> params = new ArrayList<Object>();
            params.add(token);
            System.out.println(sql);
            List<Custom> list = findSingleTable(sql, params, Custom.class);
            if (list != null && list.size() > 0) {
                return list.get(0);
            }
            return null;
        } catch (Exception e) {
            throw new DaoException(e, "查询单个custom时异常...");
        }
    }

    @Override
    public List<Custom> getCustoms(Map<String, Object> map) throws DaoException {
        try {
            List<Object> params = new ArrayList<Object>();
            String sql = "select * from token_cust_info where 1=1 ";
            if (map.get("custId") != null) {
                sql += " and cust_id =? ";
                params.add(map.get("custId"));
            }
            if (map.get("custName") != null) {
                sql += " and cust_name =? ";
                params.add(map.get("custName"));
            }
            if (map.get("custPass") != null) {
                sql += " and cust_pass =? ";
                params.add(map.get("custPass"));
            }
            if (map.get("accessKey") != null) {
                sql += " and access_key =? ";
                params.add(map.get("accessKey"));
            }
            if (map.get("ipAddress") != null) {
                sql += " and ip_address =? ";
                params.add(map.get("ipAddress"));
            }
            if (map.get("custStatus") != null) {
                sql += " and cust_status =? ";
                params.add(map.get("custStatus"));
            }
            if (map.get("createTime") != null) {
                sql += " and create_time =? ";
                params.add(map.get("createTime"));
            }
            if (map.get("updateTime") != null) {
                sql += " and update_time =? ";
                params.add(map.get("updateTime"));
            }
            if (map.get("validYear") != null) {
                sql += " and valid_year =? ";
                params.add(map.get("validYear"));
            }
            if (map.get("state") != null) {
                sql += " and state =? ";
                params.add(map.get("state"));
            }
            if (map != null && map.containsKey("pageIndex") && map.containsKey("pageSize")) {
                Integer index = (Integer) map.get("pageIndex");
                Integer size = (Integer) map.get("pageSize");
                Integer count = (index - 1) * size;
                sql += " LIMIT " + count + "," + size;
            }
            System.out.println(sql);
            return findSingleTable(sql, params, Custom.class);
        } catch (Exception e) {
            throw new DaoException(e, "分页查询Custom时异常...");
        }
    }

}
