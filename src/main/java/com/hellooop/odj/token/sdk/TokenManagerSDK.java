package com.hellooop.odj.token.sdk;

import com.hellooop.odj.token.manager.CustomManager;
import com.hellooop.odj.token.manager.TokenClock;
import com.hellooop.odj.token.manager.TokenManager;
import com.hellooop.odj.token.utils.DaoException;
import com.hellooop.odj.token.utils.ValidateException;

/**
 * 对外提供token SDK
 * @author lanping
 * @version 1.0
 * @date 2019-11-7 上午9:08:29
 */
public class TokenManagerSDK {
	
	/**
	 * 创建accessKey
	 * @param custName 客户名
	 * @param custPass 客户密码
	 * @param ipAddress IP地址
	 * @return
	 * @throws ValidateException
	 * @throws DaoException
	 */
	public String createAccessKey(String custName,String custPass,String ipAddress,int validYear,boolean isCreate) throws ValidateException, DaoException{
		CustomManager customManager = new CustomManager();
		return customManager.createAccessKey(custName, custPass, ipAddress,validYear,isCreate);
	}
	
	/**
     * 校验accessKey
     * @param custName
     * @param custPass
     * @param ipAddress
     * @return
     * @throws DaoException 
     * @throws ValidateException 
     */
    public boolean validAccessKey(String custName, String custPass, String ipAddress) throws DaoException{
    	CustomManager customManager = new CustomManager();
    	return customManager.validAccessKey(custName, custPass, ipAddress);
    }
	
	/**
     * 创建token
     *
     * @param accessKey    访问accessKey
     * @param relativeTime 相对时间 多长时间失效 默认7200秒（两个小时）
     * @param tokenData    json格式数据
     * @param callbackUrl  回调地址 暂未用到 可不传
     * @param ipAddress  访问IP地址 可不传
     * @return
     * @throws ValidateException
     * @throws DaoException
     */
	public String createToken(String accessKey,long relativeTime,String tokenData,String callbackUrl,String ipAddress) throws ValidateException, DaoException{
		TokenManager tokenManager = new TokenManager();
		return tokenManager.createToken(accessKey, relativeTime, tokenData, callbackUrl, ipAddress);
	}
	
	/**
	 * 校验token
	 * @param token
	 * @return
	 * @throws DaoException
	 * @throws ValidateException
	 */
	public boolean validateToken(String token) throws DaoException, ValidateException{
		TokenManager tokenManager = new TokenManager();
		return tokenManager.validateToken(token);
	}
	
	/**
	 * 启动定时清理过期token任务
	 */
	public void startClock(){
		TokenClock tokenClock = new TokenClock();
		tokenClock.start();
	}
	
	/**
	 * 清除Token 
	 * @param token
	 * @param accessKey 
	 * @param ipAddress IP地址 可空
	 * @throws ValidateException
	 * @throws DaoException
	 */
	public void cleanToken(String token,String accessKey,String ipAddress) throws ValidateException, DaoException{
		TokenManager tokenManager = new TokenManager();
		tokenManager.cleanToken(token, accessKey, ipAddress);
	}
	
	
	/**
	 * 获取token 数据
	 * @param token
	 * @param key
	 * @throws ValidateException
	 * @throws DaoException
	 */
	public String getTokenData(String token,String key) throws ValidateException, DaoException{
		TokenManager tokenManager = new TokenManager();
		return tokenManager.getTokenData(token, key);
	}
	
	/**
	 * 设置token数据
	 * @param token
	 * @param key
	 * @param value
	 * @throws ValidateException
	 * @throws DaoException
	 */
	public void setTokenData(String token, String key, String value)
		    throws ValidateException, DaoException{
		TokenManager tokenManager = new TokenManager();
		tokenManager.setTokenData(token, key,value);
	}
	
}
