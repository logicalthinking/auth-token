package com.hellooop.odj.token;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.alibaba.fastjson.JSONObject;
import com.hellooop.odj.token.bean.Token;
import com.hellooop.odj.token.manager.CustomManager;
import com.hellooop.odj.token.manager.TokenManager;
import com.hellooop.odj.token.utils.DaoException;
import com.hellooop.odj.token.utils.ValidateException;

public class TokenTest {

	/**
	 * 创建accessKey 需要客户名称 客户密码 作为系统唯一标识
	 */
    @Test
    public void createAccessKeyTest(){
        try {
			CustomManager customManager = new CustomManager();
			String custName = "logicalthinking";//客户名称
			String custPass = "2019&logic"; //客户密码
			String ipAddress = InetAddress.getLocalHost().getHostAddress(); //IP地址 可空

			String accessKey = customManager.createAccessKey(custName, custPass, ipAddress,1,true);
			System.out.println(accessKey);
		} catch (ValidateException e) {
			e.printStackTrace();
		} catch (DaoException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
    }

    /**
     * 创建token 需要创建的accessKey来生成token
     */
    @Test
    public void createTokenTest() {
    	try {
	        String accessKey = "C0923FDF7944A910C450F562DF879C8F";
	        TokenManager tokenManager = new TokenManager();
	        long relativeTime = 7200L;
	        JSONObject jsonObject = new JSONObject();
	        jsonObject.put("userId", "8743875353534534543");
	        String tokenData = jsonObject.toJSONString();
	        String callbackUrl = ""; //token过期会回调通知客户端 可空
	        String ipAddress = InetAddress.getLocalHost().getHostAddress(); //可空
            String token = tokenManager.createToken(accessKey, relativeTime,
            		tokenData, callbackUrl,ipAddress);
            System.out.println(token);
        } catch (DaoException e) {
            e.printStackTrace();
        } catch (ValidateException e) {
			e.printStackTrace();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
    }

    /**
     * 获取token对象
     */
    @Test
    public void getTokenTest() {
        String token = "c8fd54a8-367c-4dff-a968-a35f2da993cf";
        TokenManager tokenManager = new TokenManager();
        try {
            Token tokenObj = tokenManager.getToken(token);
            System.out.println(tokenObj);
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    /**
     * 获取token对象集合
     */
    @Test
    public void getTokensTest() {
        //String token = "e6690a67-a7df-4533-bb36-ed5595463b46";
    	String state = "Y";
        TokenManager tokenManager = new TokenManager();
        try {
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("state", state);
            //map.put("token", token);
            List<Token> tokens = tokenManager.getTokens(map);
            System.out.println(tokens);
        } catch (DaoException e) {
            e.printStackTrace();
        }
    }

    /**
     * 校验token
     */
    @Test
    public void validateTokenTest() {
        String token = "c8fd54a8-367c-4dff-a968-a35f2da993cf";
        TokenManager tokenManager = new TokenManager();
        try {
            boolean flag = tokenManager.validateToken(token);
            System.out.println(flag);
        } catch (DaoException e) {
            e.printStackTrace();
        } catch (ValidateException e) {
            e.printStackTrace();
        }
    }

    /**
     * 设置tokenData值
     */
    @Test
    public void setTokenDataTest() {
        String token = "e6690a67-a7df-4533-bb36-ed5595463b46";
        TokenManager tokenManager = new TokenManager();
        try {
            tokenManager.setTokenData(token, "userId", "sss");
        } catch (DaoException e) {
            e.printStackTrace();
        } catch (ValidateException e) {
			e.printStackTrace();
		}
    }

    /**
     * 获取tokenData值
     */
    @Test
    public void getTokenDataTest() {
        String token = "e6690a67-a7df-4533-bb36-ed5595463b46";
        TokenManager tokenManager = new TokenManager();
        try {
            String userId = tokenManager.getTokenData(token, "userId");
            System.out.println(userId);
        } catch (DaoException e) {
            e.printStackTrace();
        } catch (ValidateException e) {
			e.printStackTrace();
		}
    }

    /**
     * 清除token
     */
    @Test
    public void cleanTokenTest() {
        boolean flag = false;
        String token = "e6690a67-a7df-4533-bb36-ed5595463b46";
        String accessKey = "C0923FDF7944A910C450F562DF879C8F";
        TokenManager tokenManager = new TokenManager();
        try {
        	String ipAddress = InetAddress.getLocalHost().getHostAddress();
        	
            tokenManager.cleanToken(token,accessKey,ipAddress);
            flag = true;
        } catch (DaoException e) {
            e.printStackTrace();
        }catch (ValidateException e) {
            e.printStackTrace();
        } catch (UnknownHostException e) {
			e.printStackTrace();
		}
        Assert.assertTrue(flag);
    }
}
